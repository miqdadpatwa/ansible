# Veon Deployment Automation project
## How to build package

1.  Download zip file from main project page https://gitlab.corp.amdocs.com/PAVLOVA/ansible to linux machine
2.  Unzip ansible-master.zip and rename ansible-master folder to ansible
3.  Navigate to ansible/ansible_build folder
4.  Execute **build.sh { full | light }** script, which supports 2 modes:

*  **full** will pack all relevant files and folders and will result in ~400 Mb self extracting archive **ansible_install.bin**

*  **light** will pack everything except packages folder, so it can be used to update playbooks/defaults/etc on previously installed ansible controllers, where we already have **_/home/devops/ansible/packages_** folder. **ansible_install_light.bin** installer size is ~33 Mb

Package will be stored in ***ansible_build*** directory

## How to install package
##### Installation package will do the following:
* Create local devops/devops user with sudo privilidges
* Install ansible and dependent packages
* Extract ansible playbooks and 3p packages to /home/devops/ansible
* Put current machine IP address into default inventory for ansible and Jenkins hosts
* Install java, create yum repository at /home/devops/repos (available at http://\<IP_ADDRESS\>/CBS)
* Will generate inventory based on UPM provisioning and switch inventory from default to newly created inventory
* Install python, create devops/devops user with sudo privilidges, add yum repository in target servers
* Install and configure Jenkins (available at http://\<IP_ADDRESS\>:8080)

##### Install using one of the installation modes:

1.  Interactive mode will ask for UPM VIP address. Please provide cluster or LBA IP of MANAGER

    **./ansible_install*.bin**

2.  In non-interactive mode UPM VIP address should be supplied to installer. Please provide cluster or LBA IP of MANAGER

    **./ansible_install*.bin 10.230.26.151**

3.  Deployment mode will result in 'deployment' inventory installation with empty hosts, but repository and jenkins installed

    **./ansible_install*.bin 127.0.0.1**

## How to use ansible
##### Ansible folder with playbooks will be stored under **_/home/devops/ansible_**

```
su - devops
cd /home/devops/ansible
```

##### Note! Playbooks will be executed against inventory configured in ansible.cfg
##### Add key “-i inventories/\<site_name\>” to execute against different inventory

For example:

**ansible-playbook -i inventory/deployment init_prepare_os.yml**

* Execute bellow playbook to create yum repository and ansible inventory based on UPM provisioning. Playbook will prompt for VIP UPM IP for the first ever run.

**ansible-playbook -i inventory/\<site_name\> init_ansible.yml**

_Note1: After execution of the playbook, inventory in ansible.cfg will be changed to the site name that was provisioned in UPM_

_Note2: Run playbook against default inventory using key “-i inventories/default” if you need to reinit inventory from scratch or execute playbook against different site/upm_

**_Manually verify inventories/\<site_name\>/hosts file_**

* Install python, sudo, openssl, crete devops user and add yum repository in target servers:

**ansible-playbook -i inventory/\<site_name\> init_prepare_os.yml**

* Update **release** and **TC** version to desired one in the following file:

**/home/devops/ansible/inventories/\<site_name\>/group_vars/all/release.yml**

* Upload release folder **as devops user** to:

**/home/devops/repos/CBS**
