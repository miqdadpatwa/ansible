#!/bin/bash

if [ -z "$1" ]
  then
    echo "No argument supplied, running interactive installation"
    extra_vars=
  else
    extra_vars=upm_var=$1
fi

su - devops -c "cd ansible && ansible-playbook init_ansible.yml --extra-vars="${extra_vars}"" && \
su - devops -c 'cd ansible && ansible-playbook init_prepare_os.yml' && \
su - devops -c 'cd ansible && ansible-playbook init_jenkins.yml --tags=jenkins_init'
