#/bin/bash
echo 'Extracting ansible files...'
tar -zxvf ansible.tgz -C /home/devops && \
chown -R devops:devops /home/devops/ansible && \
chmod -R 755 /home/devops/ansible && \
echo 'Extract done' || echo 'Extract failed!'
