#!/bin/bash

if [ -z "$1" ]
  then
    echo "No argument supplied, running interactive installation"
    extra_vars=
  else
    extra_vars=$1
fi

sh 1_install_ansible.sh && \
sh 2_tune_sshd.sh && \
sh 3_create_devops.sh && \
sh 4_copy_to_devops.sh && \
sh 5_tune_default_inventory.sh && \
sh 6_init_ansible_jenkins.sh $extra_vars && \
echo 'SUCCESS' || echo 'FAIL'