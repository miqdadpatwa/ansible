#!/bin/bash

export CI_PROJECT_BUILD_DIR=`pwd`
export CI_PROJECT_DIR="$(dirname $CI_PROJECT_BUILD_DIR)"
export CI_PROJECT_BASENAME="$(basename $CI_PROJECT_DIR)"
export CI_PROJECT_TOP_DIR="$(dirname $CI_PROJECT_DIR)"
export CI_PROJECT_INSTALL_DIR="$(dirname $CI_PROJECT_BUILD_DIR)/ansible_install"

if [ "$CI_PROJECT_BASENAME" != "ansible" ]
  then
    echo "Rename $CI_PROJECT_DIR directory to $CI_PROJECT_TOP_DIR/ansible and rerun the script from new location!"
    echo "mv $CI_PROJECT_DIR $CI_PROJECT_TOP_DIR/ansible && cd $CI_PROJECT_TOP_DIR/ansible/ansible_build"
    exit 1
fi  

case $1 in
  'full')
    tar -zcvf $CI_PROJECT_INSTALL_DIR/ansible.tgz -C $CI_PROJECT_TOP_DIR ansible --exclude-from=$CI_PROJECT_BUILD_DIR/exclude.txt --exclude=ansible_install &&
    ls -lah $CI_PROJECT_INSTALL_DIR/ansible.tgz && du -sh $CI_PROJECT_INSTALL_DIR/ansible.tgz &&
    cd $CI_PROJECT_BUILD_DIR/makeself &&
    ./makeself.sh --tar-extra "--exclude-from=$CI_PROJECT_BUILD_DIR/exclude.txt" $CI_PROJECT_INSTALL_DIR $CI_PROJECT_BUILD_DIR/ansible_install.bin "Ansible inataller" ./autorun.sh &&
    rm -rf $CI_PROJECT_INSTALL_DIR/ansible.tgz &&
    du -sh $CI_PROJECT_BUILD_DIR/ansible_install.bin
  ;;

  'light')
    tar -zcvf $CI_PROJECT_INSTALL_DIR/ansible.tgz -C $CI_PROJECT_TOP_DIR ansible --exclude-from=$CI_PROJECT_BUILD_DIR/exclude_light.txt --exclude=ansible_install &&
    ls -lah $CI_PROJECT_INSTALL_DIR/ansible.tgz && du -sh $CI_PROJECT_INSTALL_DIR/ansible.tgz &&
    cd $CI_PROJECT_BUILD_DIR/makeself &&
    ./makeself.sh --tar-extra "--exclude-from=$CI_PROJECT_BUILD_DIR/exclude.txt" $CI_PROJECT_INSTALL_DIR $CI_PROJECT_BUILD_DIR/ansible_install_light.bin "Ansible inataller" ./autorun.sh &&
    rm -rf $CI_PROJECT_INSTALL_DIR/ansible.tgz &&
    du -sh $CI_PROJECT_BUILD_DIR/ansible_install_light.bin
  ;;

  *)
    echo "Usage: $0 { full | light }"
    exit 1
  ;;
esac


#Content for GitLab .gitlab-ci.yml

#tar -zcvf $CI_PROJECT_DIR/ansible_install/ansible.tgz $CI_PROJECT_DIR/../ansible --exclude=*.git* --exclude=.vscode --exclude=ansible_install #--exclude=packages
#ls -lah $CI_PROJECT_DIR/ansible_install/ansible.tgz
#cd $CI_PROJECT_DIR/ansible_install/makeself
#./makeself.sh --tar-extra "--exclude-from=exclude.txt" $CI_PROJECT_DIR/ansible_install $CI_PROJECT_DIR/ansible_install_light.bin "Ansible installation kit" #./autorun.sh
#chmod 755 $CI_PROJECT_DIR/ansible_install_light.bin
#ls -lah $CI_PROJECT_DIR/ansible_install_light.bin
