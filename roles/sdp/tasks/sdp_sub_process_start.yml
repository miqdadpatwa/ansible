# Initial version: Alexander Pavlov
---
  - name: Check agent status on SDP servers 
    shell: |
      {{ jboss_bin }}/agent status
    become: yes
    ignore_errors: yes
    register: agent_status

  - name: Start agent on SDP servers if it's not running
    shell: |
      source /root/.bashrc
      {{ jboss_bin }}/agent start
    become: yes
    ignore_errors: yes
    when: agent_status.stdout is search ("not running")

  - name: Wait 5 minutes for UPA to come up
    wait_for:
      timeout: 300
    when: agent_status.stdout is search ("not running")

  - name: Disable maintenance in UPA
    shell: |
      {{ mshell }} {{ mshell_login }} maintenance_mode -d
    register: mm_disable_out
    become: yes
    failed_when: '"a valid" in mm_disable_out.stdout or "unsuccessful" in mm_disable_out.stdout or "locate object method" in mm_disable_out.stderr'

  - debug:
      msg: "{{ mm_disable_out.stdout_lines[-1] }}"

  - name: Reload jobs and workflows in UPA
    shell: |
      {{ mshell }} {{ mshell_login }} reload_process -g application
      {{ mshell }} {{ mshell_login }} reload_job -g {{ item }}
      {{ mshell }} {{ mshell_login }} reload_workflow -g {{ item }}
    loop:
      - database
      - application
      - log
    register: reload_status
    become: yes
    failed_when: '"a valid" in reload_status.stdout or "unsuccessful" in reload_status.stdout or "locate object method" in reload_status.stderr'

  - name: Enable Process Start
    shell: |
      {{ mshell }} {{ mshell_login }} enable_process_start
    become: yes
    register: enable_process_start_out
    failed_when: '"a valid" in enable_process_start_out.stdout or "unsuccessful" in enable_process_start_out.stdout or "locate object method" in enable_process_start_out.stderr'

  - debug:
      msg: "{{ enable_process_start_out.stdout_lines[-1] | trim }}"

  - name: Enable and start CDR-MTR workflow in UPA
    shell: |
      {{ mshell }} {{ mshell_login }} enable_workflow -g database -j cdr-mtr_transfer
      {{ mshell }} {{ mshell_login }} execute_workflow -g database -j cdr-mtr_transfer      
    register: cdr_mtr_status
    become: yes
    failed_when: '"a valid" in cdr_mtr_status.stdout or "unsuccessful" in cdr_mtr_status.stdout or "locate object method" in cdr_mtr_status.stderr'

  - debug:
      msg: "{{ cdr_mtr_status.stdout_lines[-1] }}"
      
  - name: Start Continuous Processes
    shell: |
      {{ mshell }} {{ mshell_login }} enable_workflow -g application -j continuous_process_start
      {{ mshell }} {{ mshell_login }} reload_workflow -g application -j continuous_process_start
      {{ mshell }} {{ mshell_login }} execute_workflow -g application -j continuous_process_start
    become: yes
    register: continuous_process_start_out
    failed_when: '"a valid" in continuous_process_start_out.stdout or "unsuccessful" in continuous_process_start_out.stdout or "locate object method" in continuous_process_start_out.stderr'
    ignore_errors: yes

  - debug:
      msg: "{{ continuous_process_start_out.stdout_lines }}"

  - name: Wait for Continuous Processes to finish
    shell: |
      {{ mshell }} {{ mshell_login }} list_running_workflows -g application | grep continuous_process_start
    become: yes
    register: continuous_process_start_status
    until: continuous_process_start_status.stdout.find("COMPLETE") != -1
    retries: 10
    delay: 20
    ignore_errors: yes

  - name: Verify that processes started in UPA
    shell: |
      {{ mshell }} {{ mshell_login }} list_running_processes | grep sdp
    register: mshell_process_status
    become: yes
    failed_when: '"a valid" in mshell_process_status.stdout or "unsuccessful" in mshell_process_status.stdout or "locate object method" in mshell_process_status.stderr'

  - debug:
      msg: "{{ mshell_process_status.stdout_lines }}"

  - name: Verify that processes present at OS level
    shell:
      ps -aef|grep -v grep|egrep -i 'tsp|rcs|rechargeProxy|sy_dequeue'
    become: yes
    register: os_process
    failed_when: os_process.rc >= 2

  - debug:
      msg: "{{ os_process.stdout_lines }}"

  - name: Get ENABLE_SY_EVENT_NOTIF value from MAIN database
    shell:
      echo "select CHAR_VALUE from system_parameters where parameter_name='ENABLE_SY_EVENT_NOTIF';"| {{ sql_command }} {{ sql_login }}@main
    become: yes
    become_user: oracle8
    register: sql_out

  - name: Start TSP process
    shell: |
      . /staging/billing/envSetting.env
      cd $ARBORDIR/bin/
      ./tsp -nonomni -tspname tsprcs01 -cache_duration 365 -SHM $SHM -start -log RcsTspLog_tsprcs01 &
    register: tsp_status
    become: yes
    become_user: cbsuser
    when: os_process.stdout is not search('tsp -nonomni -tspname tsprcs')

  - name: Get SERVER_ID from MAIN database
    shell:
      echo "select SERVER_ID from server_definition where DSQUERY in (select GLOBAL_NAME from global_name);"| {{ sql_command }} {{ sql_login }}@main
    become: yes
    become_user: oracle8
    register: server_id
    when: os_process.stdout is not search('ure -R -ratingdb')

  - name: Start RCS process
    shell: |
      . /staging/billing/envSetting.env
      cd $ARBORDIR/bin/
      ./ure -R -ratingdb {{ (server_id.stdout_lines[-1]) | trim }} -SHM $SHM -billingdb {{ (server_id.stdout_lines[-1]) | trim }} -server -rcsname rcs01 -fullmask -log ure_onDemand_MAIN{{ (server_id.stdout_lines[-1]) | trim }}.log &
    register: rcs_status
    become: yes
    become_user: cbsuser
    when: os_process.stdout is not search('ure -R -ratingdb')

  - name: Start Recharge Proxy process
    shell: |
      /oracle/oracle8/rchg_proxy/rechargeProxyStart.sh
    register: recharge_status
    become: yes
    become_user: oracle8
    when: os_process.stdout is not search('rechargeProxy')

  - name: Start SY dequeue process
    shell: |
      /oracle/oracle8/bin/sy_dequeue_Start.sh
    register: sy_status
    become: yes
    become_user: btchuser
    when: os_process.stdout is not search('sy_dequeue') and sql_out.stdout_lines[-1] == "1"

  - name: Wait 10 seconds for processes to come up
    wait_for:
      timeout: 10

  - name: Double verify that processes present at OS level after retry
    shell:
      ps -aef|grep -v grep|egrep -i 'tsp|rcs|rechargeProxy|sy_dequeue'
    become: yes
    register: os_process_retry
    failed_when: os_process_retry.rc >= 2

  - debug:
      msg: "{{ os_process_retry.stdout_lines }}"

  - name: Fail play if one of SDP processes didn't come up
    fail:
      msg: "One or more processes failed to start. Manual investigation required"
    when: os_process_retry.stdout is not search('tsp -nonomni -tspname tsprcs') or os_process_retry.stdout is not search('ure -R -ratingdb') or os_process_retry.stdout is not search('rechargeProxy') or (os_process_retry.stdout is not search('sy_dequeue') and sql_out.stdout_lines[-1] == "1")