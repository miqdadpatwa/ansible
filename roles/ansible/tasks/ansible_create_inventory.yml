# Initial version: Alexander Pavlov
---
# name: create inventory based on UPM provisioning

    - set_fact:
        upm_ip: "{{ upm_var | default([]) }}"
      when: "upm_var is defined"

    - set_fact:
        upm_ip: "{{ hostvars['upm_vip'].ansible_host }}"
      when: "upm_var is undefined and groups['manager'] | select('search','upm_vip') | list | length != 0"

    - name: Get VIP UPM IP or OAM LBA VIP from user
      pause:
        prompt: "Type VIP IP address of UPM or hit 'Enter' if you don't want to configure anything"
      when: (groups['manager'] | select('search','upm_vip') | list | length == 0 ) and
            (groups['oam'] | select('search','oam') | list | length == 0 ) and
            upm_var is undefined
      register: input

    - set_fact:
        upm_ip: "{{ (input.user_input | length > 0) | ternary(input.user_input, '127.0.0.1') }}"
      when: (groups['manager'] | select('search','upm_vip') | list | length == 0 ) and
            (groups['oam'] | select('search','oam') | list | length == 0 ) and
            upm_var is undefined
            
    - name: Get Site name from mshell to create inventory
      local_action:
        shell
          sshpass -p '{{ ansible_ssh_pass }}' ssh -o 'StrictHostKeyChecking no' -o ConnectTimeout=10 root@{{ upm_ip }} \
          "{{ mshell }} {{ mshell_login }} list_site|awk 'NR>4 {print tolower(\$2)}'|uniq"
      register: inventory_name
      when: '"127.0.0.1" not in upm_ip'
      ignore_errors: yes

    - set_fact:
        inventory_name: "{{ (inventory_name.stdout | length > 0) | ternary(inventory_name.stdout, 'deployment') }}"
      when:
        - '"127.0.0.1" not in upm_ip'
        - '"Permission denied, please try again." not in inventory_name.stderr'

    - set_fact:
        inventory_name: "deployment"
      when: '"127.0.0.1" in upm_ip'

    - name: Fail task if login to UPM failed
      fail:
        msg: "Can't connect with {{ ansible_ssh_pass }} password, trying other one"
      when:
        - 'inventory_name.stderr is defined'
        - '"Permission denied, please try again." in inventory_name.stderr'

    - name: Preserve previous "{{ inventory_name }}" inventory folder to "{{ inventory_name }}_{{ ansible_date_time.date }}.tar.gz"
      archive:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}"
        dest: "{{ inventory_dir | dirname }}/{{ inventory_name }}_{{ ansible_date_time.date }}.tar.gz"
        owner: "devops"
        group: "devops"
        mode: 0755
      when: "groups['manager'] | select('search','upm_vip') | list | length != 0"
      become: yes

    - name: Create inventory directory "{{ inventory_name }}"
      file:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}"
        state: directory
        mode: 0755
        owner: "devops"
        group: "devops"
      become: yes

    - name: Copy group_vars to new "{{ inventory_name }}" inventory folder
      copy:
        src: "{{ inventory_dir | dirname }}/default/group_vars"
        dest: "{{ inventory_dir | dirname }}/{{ inventory_name }}/"
        mode: 0755
        owner: "devops"
        group: "devops"
      become: yes

    - name: Copy default hosts template with required group variables to new "{{ inventory_name }}" inventory folder
      copy:
        src: "{{ inventory_dir | dirname }}/default/hosts"
        dest: "{{ inventory_dir | dirname }}/{{ inventory_name }}/hosts"
        mode: 0755
        owner: "devops"
        group: "devops"
      become: yes

    - name: Change inventory to newly created {{ inventory_name }} in ansible.cfg
      replace:
        path: "{{ playbook_dir }}/ansible.cfg"
        regexp: 'inventory = inventories/.*$'
        replace: 'inventory = inventories/{{ inventory_name }}'
        backup: yes
      become: yes

    - debug:
        msg:
          - Note that inventory is now switched to "{{ inventory_name }}" in {{ playbook_dir }}/ansible.cfg
          - If new inventory from scratch is required, run 'ansible-playbook -i inventories/default init_ansible.yml'
          - Now run 'ansible-playbook init_prepare_os.yml' to prepare target servers and complete inventory
      when: '"127.0.0.1" in upm_ip or "deployment" in inventory_name'

    - meta: end_play
      when: '"127.0.0.1" in upm_ip or "deployment" in inventory_name'
      
    - name: Add VIP UPM into hosts file based on user's input for "{{ inventory_name }}" inventory
      ini_file:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}/hosts"
        section: "manager"
        option: "upm_vip ansible_host"
        value: "{{ upm_ip }}"
        mode: 0755
        no_extra_spaces: yes
        owner: devops
        backup: no
      become: yes

    - name: Get UPM cluster from mshell to update inventory
      local_action:
        shell
          sshpass -p '{{ ansible_ssh_pass }}' ssh -o 'StrictHostKeyChecking no' root@{{ upm_ip }} \
          {{ mshell }} {{ mshell_login }} list_db_cluster|grep -i MANAGER|awk '{print tolower($2)","tolower($3)","$6}'
      register: upm_clusters

    - set_fact:
        upm_group_name_tmp: "{{ item.split(',')[1] | regex_replace('[(\\b\\d+\\b)].*','') | regex_replace('upmdb', 'manager')| join ('') }}"
        upm_host_name_tmp: "{{ (item.split(',')[1]).split('-') | join('') | regex_replace('upmdb', 'upm_vip')| join ('') }}"
        upm_ansible_host_tmp: "{{ item.split(',')[2] }}"
      loop: "{{ upm_clusters.stdout_lines }}"
      register: upm_hosts_facts

    - name: Add UPM VIP into hosts file for "{{ inventory_name }}" inventory
      ini_file:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}/hosts"
        section: "{{ item.upm_group_name_tmp }}"
        option: "{{ item.upm_host_name_tmp }} ansible_host"
        value: "{{ item.upm_ansible_host_tmp }}"
        mode: 0755
        no_extra_spaces: yes
        owner: devops
        backup: no
      become: yes
      with_items: "{{ upm_hosts_facts | json_query('results[*].ansible_facts') }}"

    - name: Get UPM nodes from mshell to update inventory
      local_action:
        shell
          sshpass -p '{{ ansible_ssh_pass }}' ssh -o 'StrictHostKeyChecking no' root@{{ upm_ip }} \
          {{ mshell }} {{ mshell_login }} list_nodes|grep -i MANAGER|grep -i UPM|awk '{print tolower($2)","tolower($4)","$5}'
      register: upm_nodes

    - set_fact:
        upm_host_name_tmp: "{{ (item.split(',')[1]).split('-') | join('') | regex_replace('upmdb', 'upm_vip')| join ('') }}"
        upm_ansible_host_tmp: "{{ item.split(',')[2] }}"
      loop: "{{ upm_nodes.stdout_lines }}"
      register: upm_nodes_facts

    - name: Add UPM nodes into hosts file for "{{ inventory_name }}" inventory
      ini_file:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}/hosts"
        section: "upm"
        option: "{{ item.upm_host_name_tmp }} ansible_host"
        value: "{{ item.upm_ansible_host_tmp }}"
        mode: 0755
        no_extra_spaces: yes
        owner: devops
        backup: no
      become: yes
      with_items: "{{ upm_nodes_facts | json_query('results[*].ansible_facts') }}"

    - name: Get OAM nodes from mshell to update inventory
      local_action:
        shell
          sshpass -p '{{ ansible_ssh_pass }}' ssh -o 'StrictHostKeyChecking no' root@{{ upm_ip }} \
          {{ mshell }} {{ mshell_login }} list_nodes|grep -i MANAGER|grep -i oam|awk '{print tolower($2)","tolower($4)","$5}'
      register: oam_nodes

    - set_fact:
        oam_host_name_tmp: "{{ (item.split(',')[1]).split('-') | join('') | regex_replace('upmdb', 'upm_vip')| join ('') }}"
        oam_ansible_host_tmp: "{{ item.split(',')[2] }}"
      loop: "{{ oam_nodes.stdout_lines }}"
      register: oam_nodes_facts

    - name: Add OAM nodes into hosts file for "{{ inventory_name }}" inventory
      ini_file:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}/hosts"
        section: "oam"
        option: "{{ item.oam_host_name_tmp }} ansible_host"
        value: "{{ item.oam_ansible_host_tmp }}"
        mode: 0755
        no_extra_spaces: yes
        owner: devops
        backup: no
      become: yes
      with_items: "{{ oam_nodes_facts | json_query('results[*].ansible_facts') }}"

    - name: Get Nodeclass, Nodename and Nodeinstance to create hosts (except SDP, DTR, JMS)
      local_action:
        shell
          sshpass -p '{{ ansible_ssh_pass }}' ssh -o 'StrictHostKeyChecking no' root@{{ upm_ip }} \
          "{{ mshell }} {{ mshell_login }} list_nodes|grep -vi SDP|grep -vi DTR|grep -vi JMS|column -t|awk 'NR>3 {print tolower(\$3)","\$4","\$5}'"
      register: hosts_list
      ignore_errors: yes

    - set_fact:
        group_name_tmp: "{{ item.split()[1] | regex_replace('[(\\b\\d+\\b)].*','') | join ('') }}"
        host_name_tmp: "{{ (item.split()[1]) }}"
        ansible_host_tmp: "{{ item.split()[2] }}"
      loop: "{{ hosts_list.stdout_lines }}"
      when: hosts_list.stdout != ""
      register: hosts_facts

    - debug:
        msg: "{{ hosts_facts | json_query('results[*].ansible_facts') }}"
        verbosity: 2
      when: hosts_list.stdout != ""

    - name: Add gathered hosts into "{{ inventory_name }}" inventory (except SDP, DTR, JMS)
      ini_file:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}/hosts"
        section: "{{ item.group_name_tmp }}"
        option: "{{ item.host_name_tmp }} ansible_host"
        value: "{{ item.ansible_host_tmp }}"
        mode: 0755
        no_extra_spaces: yes
        owner: devops
        backup: no
      become: yes
      with_items: "{{ hosts_facts | json_query('results[*].ansible_facts') }}"
      when: hosts_list.stdout != ""

    - name: Get JMS hosts
      local_action:
        shell
          sshpass -p '{{ ansible_ssh_pass }}' ssh -o 'StrictHostKeyChecking no' root@{{ upm_ip }} \
          "{{ mshell }} {{ mshell_login }} list_nodes|grep -i JMS|column -t|awk '{print tolower(\$3)","\$4","\$5}'"
      register: jms_hosts_list

    - set_fact:
        jms_group_name_tmp: "{{ item.split()[1] | regex_replace('[(\\b\\d+\\b)].*','') | join ('') }}"
        jms_host_name_tmp: "{{ (item.split()[1]) }}"
        jms_ansible_host_tmp: "{{ item.split()[2] }}"
      loop: "{{ jms_hosts_list.stdout_lines }}"
      when: jms_hosts_list.stdout != ""
      register: jms_hosts_facts

    - debug:
        msg: "{{ jms_hosts_facts | json_query('results[*].ansible_facts') }}"
        verbosity: 2
      when: jms_hosts_list.stdout != ""

    - name: Add gathered JMS hosts into "{{ inventory_name }}" inventory
      ini_file:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}/hosts"
        section: "ajms"
        option: "{{ item.jms_host_name_tmp }} ansible_host"
        value: "{{ item.jms_ansible_host_tmp }}"
        mode: 0755
        no_extra_spaces: yes
        owner: devops
        backup: no
      become: yes
      with_items: "{{ jms_hosts_facts | json_query('results[*].ansible_facts') }}"
      when: jms_hosts_list.stdout != ""

    - name: Get SDP clusters from mshell to update inventory
      local_action:
        shell
          sshpass -p '{{ ansible_ssh_pass }}' ssh -o 'StrictHostKeyChecking no' root@{{ upm_ip }} \
          "{{ mshell }} {{ mshell_login }} list_db_cluster|grep -i SDP|awk 'NR>1 {print tolower(\$2)","tolower(\$3)","\$6}'"
      register: sdp_clusters
      ignore_errors: yes

    - set_fact:
        sdp_group_name_tmp: "{{ item.split()[1] | regex_replace('[(\\b\\d+\\b)].*','') | regex_replace('main', 'sdp')| join ('') }}"
        sdp_host_name_tmp: "{{ (item.split()[1]).split('-') | join('') | regex_replace('main', 'sdp')| join ('') }}"
        sdp_ansible_host_tmp: "{{ item.split()[2] }}"
      loop: "{{ sdp_clusters.stdout_lines }}"
      when: sdp_clusters.stdout != ""
      register: sdp_hosts_facts

    - name: Add SDPs into hosts file for "{{ inventory_name }}" inventory
      ini_file:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}/hosts"
        section: "{{ item.sdp_group_name_tmp }}"
        option: "{{ item.sdp_host_name_tmp }} ansible_host"
        value: "{{ item.sdp_ansible_host_tmp }}"
        mode: 0755
        no_extra_spaces: yes
        owner: devops
        backup: no
      become: yes
      with_items: "{{ sdp_hosts_facts | json_query('results[*].ansible_facts') }}"
      when: sdp_clusters.stdout != ""

    - name: Get SDP nodes from mshell to update inventory
      local_action:
        shell
          sshpass -p '{{ ansible_ssh_pass }}' ssh -o 'StrictHostKeyChecking no' root@{{ upm_ip }} \
          "{{ mshell }} {{ mshell_login }} list_db_cluster|grep -i SDP|awk 'NR>1 {print tolower(\$3),\$9}'"
      register: sdp_nodes
      ignore_errors: yes

    - set_fact:
        sdp_node_host_name_tmp: "{{ (item.split()[0]) | regex_replace('main', 'sdp')| join ('') }}"
        sdp_node_ansible_host_tmp: "{{ item.split()[1].split(',') | list | sort }}"
      loop: "{{ sdp_nodes.stdout_lines }}"
      register: sdp_nodes_hosts_facts
      when: sdp_nodes.stdout != ""

    - name: Add SDP "a" nodes into hosts file for "{{ inventory_name }}" inventory
      ini_file:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}/hosts"
        section: "sdpa"
        option: "{{ item.sdp_node_host_name_tmp }}a ansible_host"
        value: "{{ item.sdp_node_ansible_host_tmp[0] }}"
        mode: 0755
        no_extra_spaces: yes
        owner: devops
        backup: no
      become: yes
      when:
        - item.sdp_node_ansible_host_tmp[0] is defined
        - sdp_nodes.stdout != ""
      with_items: "{{ sdp_nodes_hosts_facts | json_query('results[*].ansible_facts') }}"

    - name: Add SDP "b" nodes into hosts file for "{{ inventory_name }}" inventory
      ini_file:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}/hosts"
        section: "sdpb"
        option: "{{ item.sdp_node_host_name_tmp }}b ansible_host"
        value: "{{ item.sdp_node_ansible_host_tmp[1] }}"
        mode: 0755
        no_extra_spaces: yes
        owner: devops
        backup: no
      become: yes
      when:
        - item.sdp_node_ansible_host_tmp[1] is defined
        - sdp_nodes.stdout != ""
      with_items: "{{ sdp_nodes_hosts_facts | json_query('results[*].ansible_facts') }}"

    - name: Get DTR nodes from mshell to update inventory
      local_action:
        shell
          sshpass -p '{{ ansible_ssh_pass }}' ssh -o 'StrictHostKeyChecking no' root@{{ upm_ip }} \
          "{{ mshell }} {{ mshell_login }} list_nodes|grep -i DTR|column -t|awk '{print tolower(\$3)","\$4","\$5}'"
      register: dtr_nodes

    - set_fact:
        dtr_group_name_tmp: "{{ item.split()[1] | regex_replace('[(\\b\\d+\\b)].*','') | join ('') }}"
        dtr_host_name_tmp: "{{ (item.split()[1]) }}"
        dtr_ansible_host_tmp: "{{ item.split()[2] }}"
      loop: "{{ dtr_nodes.stdout_lines }}"
      when: dtr_nodes.stdout != ""
      register: dtr_nodes_facts

    - debug:
        msg: "{{ dtr_nodes_facts | json_query('results[*].ansible_facts') }}"
        verbosity: 2
      when: dtr_nodes.stdout != ""

    - name: Add DTR "a" nodes into hosts file for "{{ inventory_name }}" inventory
      ini_file:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}/hosts"
        section: "dtra"
        option: "{{ item.dtr_host_name_tmp }} ansible_host"
        value: "{{ item.dtr_ansible_host_tmp }}"
        mode: 0755
        no_extra_spaces: yes
        owner: devops
        backup: no
      become: yes
      when:
        - "'a' in item.dtr_host_name_tmp"
        - dtr_nodes.stdout != ""
      with_items: "{{ dtr_nodes_facts | json_query('results[*].ansible_facts') }}"

    - name: Add DTR "b" nodes into hosts file for "{{ inventory_name }}" inventory
      ini_file:
        path: "{{ inventory_dir | dirname }}/{{ inventory_name }}/hosts"
        section: "dtrb"
        option: "{{ item.dtr_host_name_tmp }} ansible_host"
        value: "{{ item.dtr_ansible_host_tmp }}"
        mode: 0755
        no_extra_spaces: yes
        owner: devops
        backup: no
      become: yes
      when:
        - "'b' in item.dtr_host_name_tmp"
        - dtr_nodes.stdout != ""
      with_items: "{{ dtr_nodes_facts | json_query('results[*].ansible_facts') }}"

    - debug:
        msg:
          - Note that inventory is now switched to "{{ inventory_name }}" in {{ playbook_dir }}/ansible.cfg
          - If new inventory from scratch is required, run 'ansible-playbook -i inventories/default init_ansible.yml'
          - Now run 'ansible-playbook init_prepare_os.yml' to prepare target servers and complete inventory