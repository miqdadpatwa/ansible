# Initial version: Alexander Pavlov
---
# Setup interfaces

  - name: Setup HSBN on eth0
    lineinfile:
      path: /etc/sysconfig/network-scripts/ifcfg-eth0
      regexp: "{{ item.regexp }}"
      line: "{{ item.replace }}"
      mode: 0644
      backup: no
      create: yes
      state: present
    with_items:
      - { regexp: 'DEVICE(.*)$', replace: 'DEVICE=eth0' }
      - { regexp: 'BOOTPROTO(.*)$', replace: 'BOOTPROTO=static' }
      - { regexp: 'ONBOOT(.*)$', replace: 'ONBOOT=yes' }
      - { regexp: 'USERCTL(.*)$', replace: 'USERCTL=no' }
      - { regexp: 'HWADDR(.*)$', replace: HWADDR= }
      - { regexp: 'TYPE(.*)$', replace: 'TYPE=Ethernet' }
      - { regexp: 'IPADDR(.*)$', replace: 'IPADDR={{ ansible_default_ipv4.address }}' }
      - { regexp: 'NETMASK(.*)$', replace: 'NETMASK={{ ansible_default_ipv4.netmask }}' }
    become: yes
    when: inventory_hostname in groups['dtra'] or inventory_hostname in groups['dtrb']

  - name: Setup SLAN_a on eth1
    lineinfile:
      path: /etc/sysconfig/network-scripts/ifcfg-eth1
      regexp: "{{ item.regexp }}"
      line: "{{ item.replace }}"
      mode: 0644
      backup: no
      create: yes
      state: present
    with_items:
      - { regexp: 'DEVICE(.*)$', replace: 'DEVICE=eth1' }
      - { regexp: 'BOOTPROTO(.*)$', replace: 'BOOTPROTO=static' }
      - { regexp: 'ONBOOT(.*)$', replace: 'ONBOOT=yes' }
      - { regexp: 'USERCTL(.*)$', replace: 'USERCTL=no' }
      - { regexp: 'HWADDR(.*)$', replace: HWADDR= }
      - { regexp: 'TYPE(.*)$', replace: 'TYPE=Ethernet' }
      - { regexp: 'IPADDR(.*)$', replace: 'IPADDR={{ slan_a }}' }
      - { regexp: 'NETMASK(.*)$', replace: 'NETMASK={{ slan_mask }}' }
    become: yes
    when: inventory_hostname in groups['dtra']

  - name: Setup SLAN_b on eth1
    lineinfile:
      path: /etc/sysconfig/network-scripts/ifcfg-eth1
      regexp: "{{ item.regexp }}"
      line: "{{ item.replace }}"
      mode: 0644
      backup: no
      create: yes
      state: present
    with_items:
      - { regexp: 'DEVICE(.*)$', replace: 'DEVICE=eth1' }
      - { regexp: 'BOOTPROTO(.*)$', replace: 'BOOTPROTO=static' }
      - { regexp: 'ONBOOT(.*)$', replace: 'ONBOOT=yes' }
      - { regexp: 'USERCTL(.*)$', replace: 'USERCTL=no' }
      - { regexp: 'HWADDR(.*)$', replace: HWADDR= }
      - { regexp: 'TYPE(.*)$', replace: 'TYPE=Ethernet' }
      - { regexp: 'IPADDR(.*)$', replace: 'IPADDR={{ slan_b }}' }
      - { regexp: 'NETMASK(.*)$', replace: 'NETMASK={{ slan_mask }}' }
    become: yes
    when: inventory_hostname in groups['dtrb']

  - name: Setup VIPA on eth2
    lineinfile:
      path: /etc/sysconfig/network-scripts/ifcfg-eth2
      regexp: "{{ item.regexp }}"
      line: "{{ item.replace }}"
      mode: 0644
      backup: no
      create: yes
      state: present
    with_items:
      - { regexp: 'DEVICE(.*)$', replace: 'DEVICE=eth2' }
      - { regexp: 'BOOTPROTO(.*)$', replace: 'BOOTPROTO=static' }
      - { regexp: 'ONBOOT(.*)$', replace: 'ONBOOT=yes' }
      - { regexp: 'USERCTL(.*)$', replace: 'USERCTL=no' }
      - { regexp: 'HWADDR(.*)$', replace: HWADDR= }
      - { regexp: 'TYPE(.*)$', replace: 'TYPE=Ethernet' }
      - { regexp: 'IPADDR(.*)$', replace: 'IPADDR={{ VIPA_ADDR_IP }}' }
      - { regexp: 'NETMASK(.*)$', replace: 'NETMASK={{ VIPA_ADDR_NETMASK }}' }
    become: yes
    when: inventory_hostname in groups['dtra'] or inventory_hostname in groups['dtrb']

  - name: Setup network
    lineinfile:
      path: /etc/sysconfig/network
      regexp: "{{ item.regexp }}"
      line: "{{ item.replace }}"
      mode: 0644
      backup: no
      create: yes
      state: present
    with_items:
      - { regexp: 'NETWORKING=(.*)$', replace: 'NETWORKING=yes' }
      - { regexp: 'HOSTNAME=(.*)$', replace: 'HOSTNAME={{ inventory_hostname }}' }
      - { regexp: 'NOZEROCONF=(.*)$', replace: 'NOZEROCONF=yes' }
      - { regexp: 'GATEWAY(.*)$', replace: 'GATEWAY={{ ansible_default_ipv4.gateway }}' }
    become: yes
    when: inventory_hostname in groups['dtra'] or inventory_hostname in groups['dtrb']

  - name: Restart network service
    service:
      name: network
      state: restarted
    become: yes
    ignore_errors: yes
    when: inventory_hostname in groups['dtra'] or inventory_hostname in groups['dtrb']