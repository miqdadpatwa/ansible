dtr role
=========

This role is supposed to be executed only on scratch installed DTR and SY servers.
Finally you will get fully installed and configured DTR servers, including MYSQL and TimesTen databases.

Requirements
------------
#### Target servers
Servers must be preinstalled with LinuxScore 6.9 x64 bit. At least one network interface must be configured.

In order to run dtr role you need to define the following hosts in **_inventories/deployment/hosts:_**

    [sdp]
    sdp1 ansible_host=<SDP_VIP_IP>
    sdp2 ansible_host=<SDP_VIP_IP>
    
    [manager]
    upm_vip ansible_host=<UPM_SS_VIP_ADDRESS>

    [dtr]
    dtr1 ansible_host=<VIP_IP_ADDRESS>
    
    [dtra]
    dtr1a ansible_host=<A_NODE_IP_ADDRESS>
    
    [dtrb]
    dtr1b ansible_host=<B_NODE_IP_ADDRESS>
    
    [sysrv]
    sysrv1 ansible_host=<SY_NODE_IP_ADDRESS>
    sysrv2 ansible_host=<SY_NODE_IP_ADDRESS>
 
**[manager]** and **[sdp]** records are important for tnsnames configuration, which are used by TimesTen scripts!
There are might be many SY servers defined, but **only 1 record is allowed per each dtr* group**.
Above structure must define single DTR-SY cluster. If you need to setup new cluster, please remove old records first.

#### Packages

Upload below packages (versions can be different) to **ansible** machine under **_/home/devops/repos/CBS_** as ***devops(!!!)*** user:

1.  3p rpms:

        CMV1-JDK16-1.6.0_31-2.x86_64.rpm
        CMV1-Oracle11g2-Client-11.2.0.3-2.x86_64.rpm

2.  DTR installers and packages folders:

        DTR.5.1.1.TC1.installers_current
        DTR.5.1.1.TC1.packages_current

3.  SY installers and packages folders: 

        SY_SERVER.4.0.1.TC1.installers 
        SY_SERVER.4.0.1.TC1.packages


Role Variables
--------------

Below variables have to be defined in **_roles/dtr/vars/main.yml_** in order to successfully configure cluster:

    local_realm: veon.comverse.com
    VIPA_ADDR_IP: 10.24.13.227
    VIPA_ADDR_BROADCAST: 10.24.13.239
    VIPA_ADDR_NETMASK: 255.255.255.240
    VIPA_ADDR_NIC: eth2
    slan_a: 10.20.40.5
    slan_b: 10.20.40.6
    slan_mask: 255.255.255.0
    
    # Only 2 peers are supported by default. If more peers required, define them here and add aditional "{{ lookup('vars', 'peer#') }}" rows in with_items clause in in dtr_peer.yml
    
    peer0:
      peer_host: dsr1.dra.testzone.epc.mnc099.mcc250.3gppnetwork.org
      peer_realm: dra.testzone.epc.mnc099.mcc250.3gppnetwork.org
      peer_ip: 10.154.6.38
      peer_port: 3868
      peer_initiate: "false"
    
    peer1:
      peer_host: dsr2.dra.testzone.epc.mnc099.mcc250.3gppnetwork.org
      peer_realm: dra.testzone.epc.mnc099.mcc250.3gppnetwork.org
      peer_ip: 10.154.6.55
      peer_port: 3868
      peer_initiate: "false"

Dependencies
------------

DTRs can't be configured separately from SY servers. Thus, SY machines must be defined and available via ssh. 

Example
----------------

Once inventory hosts are configured and vars are defined, execute the following to install DTR cluster:

    su - devops
    cd ansible
    ansible-playbook -i inventories/deployment dtr_sy_install.yml

Author Information
------------------

Alexander Pavlov
alexander.pavlov@amdocs.com