# Initial version: Alexey Tsygankov
# Updated for C1 version: Alexander Pavlov
---
  - name: "Create Jenkins user: {{ jenkins.devops.user.name }}"
    shell: |
      source ~/.bashrc && \
      curl -X POST "http://{{hostvars[inventory_hostname].ansible_host}}:{{ jenkins.jenkins2_http_port }}/credentials/store/system/domain/_/createCredentials" \
      --data-urlencode \
      'json={
        "": "0",
        "credentials": {
          "scope": "GLOBAL",
          "id": "jenkins.devops.user",
          "username": "{{ jenkins.devops.user.name }}",
          "password": "{{ jenkins.devops.user.password }}",
          "description": "Created by ansible PB",
          "$class": "com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl" 
          }
        }'

  - name: "Create Jenkins VIEW file {{ inventory_dir | basename }}_view.xml"
    copy:
      dest: "{{ role_path }}/files/{{ inventory_dir | basename }}_view.xml"
      content: |
        <?xml version="1.0" encoding="UTF-8"?>
        <hudson.model.ListView>
          <name>{{ inventory_dir | basename }}</name>
          <filterExecutors>false</filterExecutors>
          <filterQueue>false</filterQueue>
          <properties class="hudson.model.View$PropertyList"/>
          <jobNames>
            <comparator class="hudson.util.CaseInsensitiveComparator"/>
          </jobNames>
          <jobFilters/>
          <columns>
            <hudson.views.StatusColumn/>
            <hudson.views.WeatherColumn/>
            <hudson.views.JobColumn/>
            <hudson.views.LastSuccessColumn/>
            <hudson.views.LastFailureColumn/>
            <hudson.views.LastDurationColumn/>
            <hudson.views.BuildButtonColumn/>
            <org.jenkins.plugins.builton.BuiltOnColumn plugin="built-on-column@1.1"/>
          </columns>
          <recurse>false</recurse>
        </hudson.model.ListView>

  - name: "Create VIEW: {{ inventory_dir | basename }}"
    shell: |
      java -jar {{defaults.devops.user.home}}/jenkins-cli.jar -s http://{{hostvars[inventory_hostname].ansible_host}}:{{ jenkins.jenkins2_http_port }} get-view {{ inventory_dir | basename }} && \
      echo -e "\n\nView already exists" || \
      java -jar {{defaults.devops.user.home}}/jenkins-cli.jar -s http://{{hostvars[inventory_hostname].ansible_host}}:{{ jenkins.jenkins2_http_port }} create-view < {{ role_path }}/files/{{ inventory_dir | basename }}_view.xml

  - name: "Create Jenkins NODE file {{ inventory_dir | basename }}_{{groups[defaults.inventory_groups.ansible_inventory_group_name][0]}}_node.xml"
    copy:
      dest: "{{ role_path }}/files/{{ inventory_dir | basename }}_{{groups[defaults.inventory_groups.ansible_inventory_group_name][0]}}_node.xml"
      content: |
        <?xml version="1.0" encoding="UTF-8"?>
        <slave>
          <name>{{groups[defaults.inventory_groups.ansible_inventory_group_name][0]}}</name>
          <description></description>
          <remoteFS>{{ defaults.devops.user.home }}</remoteFS>
          <numExecutors>10</numExecutors>
          <mode>NORMAL</mode>
          <retentionStrategy class="hudson.slaves.RetentionStrategy$Always"/>
          <launcher class="hudson.plugins.sshslaves.SSHLauncher" plugin="ssh-slaves@1.26">
            <host>{{ hostvars['ansible'].ansible_host }}</host>
            <port>22</port>
            <credentialsId>jenkins.devops.user</credentialsId>
            <maxNumRetries>0</maxNumRetries>
            <retryWaitTime>0</retryWaitTime>
          </launcher>
          <label></label>
          <nodeProperties/>
        </slave>

  - name: "Create Node: {{groups[defaults.inventory_groups.ansible_inventory_group_name][0]}}"
    shell: |
      java -jar {{defaults.devops.user.home}}/jenkins-cli.jar -s http://{{hostvars[inventory_hostname].ansible_host}}:{{ jenkins.jenkins2_http_port }} get-node {{groups[defaults.inventory_groups.ansible_inventory_group_name][0]}} && \
      echo -e "\n\nNode already exists" || \
      java -jar {{defaults.devops.user.home}}/jenkins-cli.jar -s http://{{hostvars[inventory_hostname].ansible_host}}:{{ jenkins.jenkins2_http_port }} create-node < {{ role_path }}/files/{{ inventory_dir | basename }}_{{groups[defaults.inventory_groups.ansible_inventory_group_name][0]}}_node.xml

### Below jenkins_job loop requires python-jenkins

  - name: Create Jenkins JOBs...
    jenkins_job:
      config: "{{ lookup('template', '../templates/job.xml.j2') }}"
      name: "{{ inventory_dir | basename }}_{{ (item | basename | splitext)[0] }}"
      url: http://{{hostvars[inventory_hostname].ansible_host}}:{{ jenkins.jenkins2_http_port }}
      state: present
    with_fileglob: "{{jenkins.jobs_auto.path_mask}}"
    tags: reload_jobs

  - name: Add Jenkins JOBs to VIEW...
    shell: java -jar {{defaults.devops.user.home}}/jenkins-cli.jar -s http://{{hostvars[inventory_hostname].ansible_host}}:{{ jenkins.jenkins2_http_port }} \
           add-job-to-view {{ inventory_dir | basename }} {{ inventory_dir | basename }}_{{ (item | basename | splitext)[0] }}
    with_fileglob: "{{jenkins.jobs_auto.path_mask}}"
    tags: reload_jobs

  - name: Prepare Jenkins Pipelines per inventory
    template:
      src: "{{ item }}"
      dest: "{{ role_path }}/files/{{ item | basename }}"
    with_fileglob:
      - "templates/pipeline_*.txt"
    tags: reload_jobs

  - name: Create Jenkins Pipelines
    jenkins_job:
      config: "{{ lookup('template', '../templates/pipeline.xml.j2') }}"
      name: "{{ inventory_dir | basename }}_A_{{ (item | basename | splitext)[0] }}"
      url: http://{{hostvars[inventory_hostname].ansible_host}}:{{ jenkins.jenkins2_http_port }}
      state: present
    vars: 
      pipeline: "{{ lookup('file', '{{ item }}') }}"
    register: pipelines
    with_fileglob:
      - "files/pipeline_*.txt"
    tags: reload_jobs

  - name: Add Jenkins Pipelines to VIEW...
    shell: java -jar {{defaults.devops.user.home}}/jenkins-cli.jar -s http://{{hostvars[inventory_hostname].ansible_host}}:{{ jenkins.jenkins2_http_port }} \
           add-job-to-view {{ inventory_dir | basename }} {{ item.name }}
    with_items: "{{ pipelines.results }}"
    no_log: yes
    tags: reload_jobs

  - name: Clean up temporary pipeline files
    file:
      path: "{{ item }}"
      state: absent
    with_fileglob:
      - "files/*.txt"
    tags: reload_jobs

  - debug:
      msg:
        - You can now access Jenkins in browser via http://{{hostvars[inventory_hostname].ansible_host}}:{{ jenkins.jenkins2_http_port }}
    tags: reload_jobs