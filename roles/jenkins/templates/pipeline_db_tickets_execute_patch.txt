pipeline {
agent none
    stages {
        stage('Execute Patch DB Tickets') {
                    steps {
                        build ('{{ inventory_dir | basename }}_db_tickets_apply_patch')
                    }
        }
    }
}
