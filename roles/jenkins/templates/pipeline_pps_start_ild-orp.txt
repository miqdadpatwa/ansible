pipeline {
    agent none
    stages {
        stage('Start PPS in parallel') {
            parallel {
                stage('Start OSA') {
                    steps {
                        build ('{{ inventory_dir | basename }}_ure_osa_flow_start_ild')
                    }
                }
                stage('Start NOTIF') {
                    steps {
                        build ('{{ inventory_dir | basename }}_ure_notif_flow_start_ild')
                    }
                }
                stage('Start Offline Raters') {
                    steps {
                        build ('{{ inventory_dir | basename }}_ofr_flow_start_ild')
                    }
                }
                stage('Start Online Raters') {
                    steps {
                        build ('{{ inventory_dir | basename }}_ure_onrater_flow_start_ild')
                    }
                }
            }
        }
    }
}
