pipeline {
agent none
    stages {
        stage('Backup and Preparations FE') {
                    steps {
                        build ('{{ inventory_dir | basename }}_fe_preparation')
                    }
        }
    }
}
