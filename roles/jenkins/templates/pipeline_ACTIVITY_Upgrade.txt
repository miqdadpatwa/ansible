pipeline {
    agent none
    stages {
        stage('Prepare site for ansible/jenkins pipelines') {
            steps {
                build ('{{ inventory_dir | basename }}_init_prepare_os')
            }
        }
        stage('Stop C1') {
            parallel {
                stage('Stop UPM') {
                    steps {
                        build ('{{ inventory_dir | basename }}_A_pipeline_upm_stop')
                    }
                }
                stage('Stop UREs and enable ORP') {
                    steps {
                        build ('{{ inventory_dir | basename }}_A_pipeline_pps_stop+orp')
                    }
                }
                stage('Stop FE servers') {
                    steps {
                        build ('{{ inventory_dir | basename }}_A_pipeline_fe_stop')
                    }
                }
            }
        }
        stage('Stop SDP') {
            steps {
                build ('{{ inventory_dir | basename }}_sdp_stop')
            }
        }
        stage('Upgrade operations') {
            parallel {
                stage('Upgrade FE servers') {
                    steps {
                        build ('{{ inventory_dir | basename }}_A_pipeline_fe_installation')
                    }
                }
                stage('Upgrade SDP servers') { 
                    stages {
                        stage('Upgrade SDP servers') {
                            steps {
                                build ('{{ inventory_dir | basename }}_sdp_upgrade')
                            }
                        }
                        stage('Restore SDP settings') {
                            steps {
                                build ('{{ inventory_dir | basename }}_sdp_restore')
                            }
                        }
                        stage('Execute Patch DB Tickets') {
                            steps {
                                build ('{{ inventory_dir | basename }}_pipeline_db_tickets_execute_patch')
                            }
                        }
                        stage('Run DB Install DB Tickets') {
                            steps {
                                build ('{{ inventory_dir | basename }}_pipeline_db_tickets_db_install')
                            }
                        }
                    }
                }
            }
        }
        stage('Start SDP') {
            steps {
                build ('{{ inventory_dir | basename }}_sdp_start')
            }
        }
        stage('Start C1') {
            parallel {
                stage('Enable and start night workflows on SDP servers') {
                    steps {
                        build ('{{ inventory_dir | basename }}_sdp_workflow_start')
                    }
                }
                stage('Start UREs and disable ORP') {
                    steps {
                        build ('{{ inventory_dir | basename }}_A_pipeline_pps_start_ild-orp')
                    }
                }
                stage('Start UREs and disable ORP') {
                    steps {
                        build ('{{ inventory_dir | basename }}_A_pipeline_pps_start_ild-orp')
                    }
                }
                stage('Start FE servers') {
                    steps {
                        build ('{{ inventory_dir | basename }}_A_pipeline_fe_start')
                    }
                }
            }
        }
    }
}