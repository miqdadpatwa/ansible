pipeline {
agent none
    stages {
        stage('Upgrade UPM') {
                    steps {
                        build ('{{ inventory_dir | basename }}_upm_installation')
                    }
        }
    }
}
